import json
import boto3
from boto3.dynamodb.conditions import Key, Attr

def lambda_handler(event, context):
    s3 = boto3.client("s3")
    dynamo_db = boto3.resource('dynamodb')
    dynamoTable = dynamo_db.Table('files')
    print(event)
    expected_file_list=[]
    
    if event:
        record = event["Records"][0]
        bucket = record['s3']['bucket']['name']
        key = str(record['s3']['object']['key'])
        alreadyProcessed = dynamoTable.query(
            KeyConditionExpression=Key('filename').eq(key),
            FilterExpression= Attr("file_processed").eq(1)
        )
        if alreadyProcessed['Items']:
           print("Already processed")
           exit(1)

        date_suffix=key.split("_")[1].split(".")[0]
        type=key.split("_")[0]
        for file in {"clients","transactions","portfolios","accounts"}:
            expected_file_list.append(f"{file}_{date_suffix}.csv")

        fileExists = dynamoTable.query(
            KeyConditionExpression=Key('filename').eq(key)
        )
        if not fileExists['Items']:
            dynamoTable.put_item(Item = {
                "filename" : key,
                "date" : date_suffix,
                "type" : type,
                "bucket": bucket,
                "file_processed": 0
            })
        else:
            print('File already exists')
            
        response = dynamoTable.scan(
            FilterExpression=Attr('filename').is_in(expected_file_list)
        )
        if response["Count"] == len(expected_file_list):
            print("The files are ready")
            print(response["Items"])
            return {
                "Items": response["Items"]
            }
        else:
            print("the files are not ready yet")
            exit(1)
            
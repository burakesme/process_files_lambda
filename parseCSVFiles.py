import json
import boto3
import uuid
from collections import namedtuple
from urllib.parse import unquote_plus

s3_client = boto3.client('s3')
        


def parse_csv_files(clients_path, portfolio_path, account_path, transaction_path):
    import csv
    client_list = []
    portfolio_list = []
    account_list = []
    transaction_list = []
    portfolio_message_list= [] 
    client_message_list= []
    with open(clients_path) as clients_file, open(portfolio_path) as portfolio_file, open(
            account_path) as account_file, open(transaction_path) as transaction_file:
        csv_reader = csv.reader(clients_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                Client = namedtuple('Client', row)
                line_count += 1
            else:
                client = Client(*row)
                client_list.append(client)
                line_count += 1
        csv_reader = csv.reader(portfolio_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                Portfolio = namedtuple('Portfolio', row)
                line_count += 1
            else:
                portfolio = Portfolio(*row)
                portfolio_list.append(portfolio)
                line_count += 1

        csv_reader = csv.reader(account_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                Account = namedtuple('Account', row)
                line_count += 1
            else:
                account = Account(*row)
                account_list.append(account)
                line_count += 1

        csv_reader = csv.reader(transaction_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                Transaction = namedtuple('Transaction', row)
                line_count += 1
            else:
                transaction = Transaction(*row)
                transaction_list.append(transaction)
                line_count += 1

    for client in client_list:
        port_client = tuple(
            filter(lambda portfolio: portfolio.client_reference == client.client_reference, portfolio_list))
        for portfolio in tuple(port_client):
            acc_client = tuple(filter(lambda account: account.account_number == portfolio.account_number, account_list))
            tra_client = tuple(
                filter(lambda transaction: transaction.account_number == portfolio.account_number, transaction_list))

            client_message = {
                "type": "client_message",
                "client_reference": client.client_reference,
                "tax_free_allowance": client.tax_free_allowance,
                "taxes_paid": sum(float(acc.taxes_paid) for acc in acc_client)
            }
            portfolio_message = {
                "type": "portfolio_message",
                "portfolio_reference": portfolio.portfolio_reference,
                "cash_balance": sum(float(acc.cash_balance) for acc in acc_client),
                "number_of_transactions": len(tra_client) + 1,
                "sum_of_deposits": sum(float(tran.amount) for tran in tra_client if tran.keyword == "DEPOSIT")
            }
            print(portfolio_message)
            portfolio_message_list.append(portfolio_message)
        print(client_message)
        client_message_list.append(client_message)
    
    return {
            "portfolio_message": portfolio_message_list,
            "client_message": client_message_list
        }

def move_processed_files(file_list, bucket, processed_bucket,date_suffix):
    dynamo_db = boto3.resource('dynamodb')
    s3 = boto3.resource("s3")
    dynamoTable = dynamo_db.Table('files')
    for key in file_list:
        try:
            copy_source = {
                'Bucket': bucket,
                'Key': key
            }
            dest_bucket = s3.Bucket(processed_bucket)
            cp_obj = dest_bucket.Object(key)
            cp_obj.copy(copy_source)
            response = dynamoTable.update_item(
                                        Key={
                                            'filename': key
                                        },
                                        UpdateExpression="set file_processed=:a",
                                        ExpressionAttributeValues={
                                            ':a': 1
                                        },
                                        ReturnValues="UPDATED_NEW"
                                        )
            rm_obj = s3.Object(bucket, key)
            rm_obj.delete()
        except:
            print("Copy Failed")
            exit 1 
    


def lambda_handler(event, context):
    file_list = []
    print(event)
    for record in event['responsePayload']["Items"]:
        bucket = record['bucket']
        key = unquote_plus(record['filename'])
        date = record["date"]
        print(key)
        if "clients" in key:
            tmpkey = key.replace('/', '')
            client_download_path = '/tmp/{}{}'.format(uuid.uuid4(), tmpkey)
            s3_client.download_file(bucket, key, client_download_path)
        elif "portfolios" in key:
            tmpkey = key.replace('/', '')
            portfolio_download_path = '/tmp/{}{}'.format(uuid.uuid4(), tmpkey)
            s3_client.download_file(bucket, key, portfolio_download_path)
        elif "accounts" in key:
            tmpkey = key.replace('/', '')
            account_download_path = '/tmp/{}{}'.format(uuid.uuid4(), tmpkey)
            s3_client.download_file(bucket, key, account_download_path)
        elif "transactions" in key:
            tmpkey = key.replace('/', '')
            transaction_download_path = '/tmp/{}{}'.format(uuid.uuid4(), tmpkey)
            s3_client.download_file(bucket, key, transaction_download_path)
        file_list.append(key)
    message_list=parse_csv_files(client_download_path, portfolio_download_path, account_download_path,
                          transaction_download_path)
    processed_bucket="processed-transactions-burakesme"
    move_processed_files(file_list, bucket,processed_bucket, date)
    return message_list